$(document).ready(function($) {
    // default behaviour
    $( ".draggable" ).draggable({containment: 'body'}).resizable();

    // window system :)
    // theres a div with an id and a link with the same name as window-data property...
    $('a').click(function(){
        var window_name = $(this).data('window');
        $('#'+window_name).show();
    });

    //default behaviour for the whole window system 
    $('.closeBtn').click(function(event) {
        $(this).closest('.window').hide();
    });

    setInterval(function(){
        var initial = parseInt($('mem').html()); 
        var next = initial - 7;
        $('mem').html(next);
        if (next < 10){
            $('mem').html('164413'); 
        }
    }, 2000); 

    console.log();
});