INSTALLATION
------------

Zum Installieren entpacken und ins Cursors-Verzeichnis kopieren, bei 
Win NT/2k also X:\WINNT\CURSORS und bei den Spiele-Wins 
X:\WINDOWS\CURSORS.

NON-WIN2K
---------

In der Systemsteuerung auf "Maus" gehen und Zeiger f�r Zeiger 
ersetzen. Dauert zwar etwas lang, gibt aber auch Genugtuung.

WIN2K
-----

Nachdem die Zeiger in C:\WINNT\CURSORS gelandet sind, einfach
auf die mitgelieferte Datei "win2000_cursors.reg" doppelklicken,
auf "Ja, ja" klicken und fertig. Anschlie�end ist das Schema
"Amiga WB 1.x" in der Systemsteuerung als Zeiger-Schema ausw�hlbar.

CREDIT
------

Pixel ............ losso@heckmeck.de
Registry-Trick ... iso@eimersaufen.de

                   http://www.heckmeck.de
