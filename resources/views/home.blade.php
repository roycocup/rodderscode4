@extends('layouts.master')

@section('content')

	<div id="about" class="window draggable">
		<div class="topBorder">
			<a class="closeBtn" href="#">
				<div class="subRect">.</div>
			</a>
			<div class="windowName">Softroad Games
				<div class="tractionLines"></div>
			</div>
		</div>
		<div class="content">
			<p>Softroad Games is a London based game producing studio. We are here because our master sent us. The password is "I-Know-Nothing".</p>
			<p>By the way, we are very bad with css, so in order to scroll down on these windows, you must use your mouse wheel.</p>
		</div>
		<div class="rightBorder"></div>
		<div class="bottomBorder"></div>
	</div>


	<div id="demos" class="window draggable">
		<div class="topBorder">
			<a class="closeBtn" href="#">
				<div class="subRect">.</div>
			</a>
			<div class="windowName">WSFB
				<div class="tractionLines"></div>
			</div>
		</div>
		<div class="content">
            <p><b>"We shall fight on the beaches"</b></p>
			<p>A disaster of a game but served the purpose to be our first game.</p>
            <p>A tower defense game - Available for PC/Mac/Android.</p>
			<p>You can download it (at your own risk) from <a href="http://softroad.itch.io/wsfb" style="color:white;" target="_blank" >itch.io</a></p>
            <a href="/assets/resources/WSFB_001.png" class="thumbnail">
                <img class="thumb" src="/assets/resources/WSFB_001.png" alt="First look at WSFB">
            </a>
		</div>
		<div class="rightBorder"></div>
		<div class="bottomBorder"></div>
	</div>


	<div class="icons">
        <div class="icon">
            <a href="#" data-window="about"><img src="/assets/images/disk.png" alt="about"></a><br>
            <span>SG</span>
        </div>
        <div class="icon">
            <a href="#" data-window="demos"><img src="/assets/images/disk.png" alt="Demos"></a><br>
            <span>WSFB</span>
        </div>
        <!-- <div class="icon">
            <a href="#" data-window="cv"><img src="/assets/images/disk.png" alt="CV"></a><br>
            <span>CV</span>
        </div> -->
    </div>
@stop
                
                
           
