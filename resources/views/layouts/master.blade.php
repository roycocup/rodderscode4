<!DOCTYPE html>
<html>
    <head>
        <title>Softroad Games</title>
        <link rel="stylesheet" href="/assets/css/bootstrap.css">
        <link rel="stylesheet" href="/assets/css/app.css">
        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    </head>
    <body>
        <nav>
            <ul>
                <li>Softroad Games</li>
                <li>Version 1.0</li>
                <li><mem>164567</mem> free memory</li>
            </ul>
            <div class="buttons">
                <div class="toback">
                    <div class="blackSquare"></div>
                    <div class="whiteSquare"></div>
                </div>
                <div class="tofront">
                    <div class="blackSquare"></div>
                    <div class="whiteSquare"></div>
                </div>    
            </div>
        </nav>
        <div class="container">
            <div class="content">
            	@yield('content')
             </div>
        </div>
        <script src="/assets/js/app.js"></script>
    </body>
</html>




